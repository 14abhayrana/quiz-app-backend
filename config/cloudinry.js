const cloudinary = require('cloudinary').vz;
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_service: process.env.CLOUDINARY_API_SERVICE,
  api_key: process.env.CLOUDINARY_API_KEY,
});

module.exports = { cloudinary };